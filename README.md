# Drop

## Prerequisites
- [Node.js](http://nodejs.org/) - install via
- [NPM](https://www.npmjs.org/) - this comes with Node.js
- [Gulp.js](http://gulpjs.com/) - `npm install -g gulp`
- [Nunjucks](https://mozilla.github.io/nunjucks/) - used as node js view engine

## Initial Setup
- run `npm install` to get all the dependencies
- run `gulp` to run localhost server (http://localhost:8000/)
- run `npm start` to run like prodcation, staging server. ( All compressed assets are served )


## Deploy
1) shipit production deploy
2) ssh ubuntu@ec2-52-55-242-106.compute-1.amazonaws.com
3) cd /srv/apps/drop/current && pkill drop_app && pkill drop_gulp
4) ps aux | grep drop_
5) /usr/bin/nohup bash -c "npm start > nohup2.out&" && sleep 5; cat nohup2.out